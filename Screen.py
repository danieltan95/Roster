#--------------------------------------------------#
#Roster Planning Program GUI Screen
#--------------------------------------------------#

from tkinter import *
from Pipe import *
class PlanningApp:
    def __init__(self):
        self.root=Tk()
        #Set pointer to person with last counter
        self.PTR=0
        self.OT=OT
        for i in range(len(self.OT.Team)):
            if self.OT.Team[i].Last:
                self.PTR=i
        self.OT.DisplayPlanning(self.OT.Planning(self.OT.Available()))
        F=Frame(self.root)
        F.pack()
        self.Title=Label(F, text=self.OT.Name)
        self.Title.pack()
        self.TA=Text(F, width=50)
        self.TA.pack()
        self.TA.insert(INSERT, self.OT.DisplayTeam(self.OT.Team))
        self.TA.configure(state='disabled')
        VP=Button(F, text="View Planning", command=self.ViewPlanning)
        VP.pack(side=LEFT)
        VT=Button(F, text="View Team", command=self.ViewTeam)
        VT.pack(side=LEFT)
        ET=Button(F, text="Edit Team", command=self.EditTeam)
        ET.pack(side=LEFT)
        B=Button(F, text="Edit Time", command=self.EditTime)
        B.pack(side=LEFT)
        B=Button(F, text="Restore Globals", command=self.RestoreGlobals)
        B.pack(side=LEFT)
        self.root.mainloop()

    def RestoreGlobals(self):
        if messagebox.askokcancel("Warning","Restore Globals?"):
            restoreG(N)
            self.OT=CreateTeam(N,D)
            messagebox.showinfo("Completed", "Globals restored\n")
            self.ViewPlanning()
            self.ViewTeam()
            
    def ViewPlanning(self):
        self.TA.config(state="normal")
        self.TA.delete(1.0, END)
        S=self.OT.DisplayPlanning(self.OT.Planning(self.OT.Available()))
        self.TA.insert(INSERT, S)
        self.TA.configure(state='disabled')

    def ViewTeam(self):
        self.TA.config(state="normal")
        self.TA.delete(1.0, END)
        S=self.OT.DisplayTeam(self.OT.Team)
        self.TA.insert(INSERT, S)
        self.TA.configure(state='disabled')

    def EditTeam(self):        
        #Internal Mechs to modify team      
        def SwitchLast():
            N=IV.get()
            self.PTR=N
            for i in range(len(self.OT.Team)):
                self.OT.Team[i].Last=False
            self.OT.Team[N].Last=True      
        def Add():
            UpdateAll()
            self.OT.AddNew("", "FF", self.OT.G)
            self.EditTeam()
        def ClearEmpty():
            UpdateAll()
            T=[]
            for i in range(len(self.OT.Team)):
                if self.OT.Team[i].Name.strip():
                    T.append(self.OT.Team[i])
            self.OT.Team=T
            self.EditTeam()        
        def UpdateAll():
            #Get all data as strings from entries
            self.OT.Name=ET.get().strip()
            self.Title["text"]=self.OT.Name
            E=[[str(S.get()).strip() for S in EG[i]] for i in range(len(self.OT.Team))]
            L=[W.Last for W in self.OT.Team ]
            self.OT.Team=[]
            for i in range(len(L)):
                self.OT.AddNew(E[i][0],
                               E[i][1] if E[i][1] else "M",
                               self.OT.G, L[i] ,
                               [int(T.strip()) for T in E[i][2].split(",") if T.isdigit()])
            self.OT.G["Name_list"]=[wk.Name for wk in self.OT.Team]
            self.OT.G["Shift_list"]=[wk.Shift for wk in self.OT.Team]
            self.OT.G["Busylist_list"]=[wk.BL for wk in self.OT.Team]
            self.OT.G["Team_name"]=self.OT.Name
            self.OT.G["Last_name"]=self.PTR
            writeG(self.OT.G, N)
            self.ViewPlanning()
            self.ViewTeam()
            if self.OT.Error:
                messagebox.showwarning("Error", self.OT.Error)
            else:
                self.T.destroy()
        #Start of GUI
        for i in range(len(self.OT.Team)):
            if self.OT.Team[i].Last:
                self.PTR=i
        self.T=Toplevel()
        F=Frame(self.T)
        F.pack()
        L=Label(F, text="Team Name: ")
        L.pack()
        ET=Entry(F, bd=4)
        ET.insert(INSERT,self.OT.Name)
        ET.bind("<Return>",self.focus_next_window)
        ET.pack()
        TXT=("Name", "Shift", "001", "Busy Hour")
        FG=[Frame(self.T) for i in range(len(TXT))]
        for F in FG:
            F.pack(side=LEFT)
        EG=[]
        BG=[]
        IV=IntVar()
        IV.set(self.PTR)
        for F in FG:
            L=Label(F, text=TXT[FG.index(F)])
            L.pack()
        for i in range(len(self.OT.Team)):
            EG.append([])
            BG.append([])
            E=Entry(FG[0], bd=4)
            E.insert(INSERT,self.OT.Team[i].Name)
            E.bind("<Return>",self.focus_next_window)
            E.pack()
            EG[i].append(E)
            E=Entry(FG[1],bd=4)
            E.insert(INSERT,self.OT.Team[i].Shift)
            E.bind("<Return>",self.focus_next_window)
            E.pack()
            EG[i].append(E)
            B=Radiobutton(FG[2], text=" ", variable=IV, value=i, command=SwitchLast)
            B.bind("<Return>",self.focus_next_window)
            B.pack()
            BG.append(B)
            E=Entry(FG[3], bd=4)
            E.insert(INSERT, ",".join([str(X) for X in self.OT.Team[i].BL]))
            E.bind("<Return>",self.focus_next_window)
            E.pack()
            EG[i].append(E)
        B=Button(self.T, text="Add New", command=Add)
        B.bind("<Return>",self.focus_next_window)
        B.pack()
        B=Button(self.T, text="Clear Empty", command=ClearEmpty)
        B.bind("<Return>",self.focus_next_window)
        B.pack()
        B=Button(self.T, text="Update", command=UpdateAll)
        B.bind("<Return>",self.focus_next_window)
        B.pack()

    def focus_next_window(self,event):
        event.widget.tk_focusNext().focus()

    def EditTime(self):
        #Internal mechs to modify times
        def Add():
            UpdateAll()
            T=self.OT.G["Rest_list"]
            T['T']=[[],"Temp"]
            self.OT.G["Rest_list"]=T
            self.EditTime()
        def ClearEmpty():
            UpdateAll()
            T={}
            for k in self.OT.G["Rest_list"]:
                if k.strip() not in ("T", ""):
                    T[k]=self.OT.G["Rest_list"][k]
            self.OT.G["Rest_list"]=T
            self.EditTime()
        def UpdateAll():
            #Get all data from entries as strings
            E=[[str(S.get()).strip() for S in EG[i]] for i in range(len(K))]
            Time_list=[int(T.strip()) for T in ET.get().strip().split(",") if T.isdigit()]
            if  Time_list:
                Rest_list={}
                for i in range(len(K)):
                    Rest_list[E[i][0]]=[[int(T.strip()) for T in E[i][1].split(",") if T.isdigit()], E[i][2]]
                self.OT.G["Time_list"]=Time_list
                self.OT.G["Rest_list"]=Rest_list
                writeG(self.OT.G, N)
                self.ViewPlanning()
                self.ViewTeam()
                self.L.destroy()
            else:
                messagebox.showwarning("Error", "Time list cannot be empty")
        #Start of GUI
        self.L=Toplevel()       
        TXT=("Key", "Rest time", "Decription")
        K=list(self.OT.G["Rest_list"].keys())
        F=Frame(self.L)
        F.pack()
        L=Label(F, text="Time list: ")
        L.pack()
        ET=Entry(F, bd=4)
        ET.insert(INSERT,",".join([str(X) for X in self.OT.G["Time_list"]]))
        ET.bind("<Return>",self.focus_next_window)
        ET.pack()
        FG=[Frame(self.L) for i in range(len(TXT))]                
        EG=[]        
        for F in FG:
            F.pack(side=LEFT)    
        for F in FG:
            L=Label(F, text=TXT[FG.index(F)])
            L.pack()
        for i in range(len(K)):
            EG.append([])
            E=Entry(FG[0], bd=4)
            E.insert(INSERT,K[i])
            E.bind("<Return>",self.focus_next_window)
            E.pack()
            EG[i].append(E)
            E=Entry(FG[1],bd=4)
            E.insert(INSERT,",".join(str(X) for X in self.OT.G["Rest_list"][K[i]][0]))
            E.bind("<Return>",self.focus_next_window)
            E.pack()
            EG[i].append(E)
            E=Entry(FG[2],bd=4)
            E.insert(INSERT,self.OT.G["Rest_list"][K[i]][1])
            E.bind("<Return>",self.focus_next_window)
            E.pack()
            EG[i].append(E)
        B=Button(self.L, text="Add New", command=Add)
        B.bind("<Return>",self.focus_next_window)
        B.pack()
        B=Button(self.L, text="Clear Empty", command=ClearEmpty)
        B.bind("<Return>",self.focus_next_window)
        B.pack()
        B=Button(self.L, text="Update", command=UpdateAll)
        B.bind("<Return>",self.focus_next_window)
        B.pack()


                    
if __name__=="__main__":
    app=PlanningApp()
