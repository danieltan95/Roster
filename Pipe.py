#--------------------------------------------------#
#Roster Planning Program Utility Pipe
#--------------------------------------------------#

from Gear import Worker, Team
from Tank import *
from pickle import dump, load
from os import remove

#Utility tools
#Saved files of form *.tnk

def write(s, name):
    with open("%s.tnk"%name, "wb") as f:
        dump(s, f)

def read(name):
    try:
        with open("%s.tnk"%name, "rb") as f:
            ans=load(f)
        return ans
    except IOError as e:
        print(e)

def crush(name):
    try:
        remove("%s.tnk"%name)
    except IOError as e:
        print(e)

def readG(N, D):
    N=N.split(",")
    N=[n.strip() for n in N]
    G=dict()
    for K in range(len(D)):
        G[N[K]]=D[K]        
    for n in N:
        if read(n):
            G[n]=read(n)
    return G

def writeG(G,N):
    N=N.split(",")
    N=[n.strip() for n in N]
    for n in N:
        write(G[n],n)

def restoreG(N):
    N=N.split(",")
    N=[n.strip() for n in N]
    for n in N:
        crush(n)

def CreateTeam(N,D):
    OT=Team(readG(N,D))
    for i in range(len(OT.G["Name_list"])):
        OT.AddNew(OT.G["Name_list"][i],OT.G["Shift_list"][i], OT.G, Busy_list=OT.G["Busylist_list"][i])
    OT.Team[OT.G["Last_name"]].Last=True
    return OT

#Piped into GUI
OT=CreateTeam(N,D)
