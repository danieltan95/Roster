#--------------------------------------------------#
#Roster Planning Program Configurations Tank
#--------------------------------------------------#
#Default values for all constants
Time_list=[12,1,2,4,5,6,7]
Rest_list={
            'M':[[2,6,7],'Morning'],
            'A':[[12,4],"Afternoon"],
            'F0':[[12,4],"Full-12"],
            'F1':[[1,5],"Full-1"],
            'F2':[[2,6],"Full-2"],
            'R':[[12,1,2,3,4,5,6,7],"Rest"],
            'FF':[[],"Empty space"]
                }
Name_list=[
                    "Zam",
                    "HuiLin",
                    "Ariel",
                    "Lily",
                    "XLing",
                    "Syamil",
                    "Daniel"
                    ]
Shift_list=[
                "R",
                "F1",
                "F1",
                "F2",
                "R",
                "A",
                "M"
                ]
Busylist_list=[
                    [],
                    [1,2],
                    [],
                    [],
                    [],
                    [],
                    []
                    ]
Team_name="Operations Team"
Last_name=Name_list.index("Ariel")
#List to arrange the constants
D=[Time_list,Rest_list, Name_list, Shift_list, Busylist_list, Team_name, Last_name]
N="Time_list,Rest_list, Name_list, Shift_list, Busylist_list, Team_name, Last_name"
