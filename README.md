Roster
======

Roster Planning program in Python

Solving people-time management.

Customisable members, shifts and rest times.

Requirements:
------------
Python 3.3 / tkinter 

Installation:
------------
Download, unpack, and do

$python Screen.py

and start editing your own team.
