#--------------------------------------------------#
#Roster Planning Program Logic Gear
#--------------------------------------------------#

from itertools import permutations

class Team:
    def __init__(self,Global):
        self.Team=[]
        self.G=Global
        self.Name=self.G["Team_name"]
        self.Error=""
        #Manpower note
        self.MP=["",""]

    def AddNew(self,Name,Shift, G, Last=False, Busy_list=None):
        #Shuffle the team so morning shifts are more likely to take counters
        if Shift =="M":
            self.Team.insert(0,Worker(Name,Shift,G,Last, Busy_list))
        else:
            self.Team.append(Worker(Name,Shift,G,Last, Busy_list))

    def Available(self):
        team=self.Team
        hours=self.G["Time_list"]
        #Times without enough people on board
        Errorlist=[]
        day=[[] for i in hours]
        for time in range(len(hours)):
            for worker in team:
                if hours[time] in worker.WL:
                    day[time].append(worker)
        correct=True
        for L in range(len(day)):
            if len(day[L])<2:
                correct=False
                Errorlist.append(str(hours[L]))
        self.Error="Not enough people at : %s"%(",".join(Errorlist)) if  not correct else ""
        day=day if correct else [[Worker("-","FF", self.G) for i in range(3)]for i in hours]
        self.MP[0]=str(len(day[0]))
        self.MP[1]=str(len(day[-1]))
        return day

    def Planning(self, AL):
        planning=[]
        DP=AL
        choicelist=permutations(DP[-1])
        for choice in choicelist:
            DP[-1]=choice
            DP[-1]=[X for X in DP[-1]]
            if len(DP[-1])<3:
                X=Worker("-", "FF", self.G)
                DP[-1].insert(1,X)
            for V in  range(len(DP)-2,-1,-1):
                for H in range(len(DP[V])):
                    #Check if the worker can take counter continuously
                    if H<len(DP[V+1]) and DP[V][H] != DP[V+1][H] and DP[V+1][H] in DP[V]:
                        DP[V].remove(DP[V+1][H])
                        DP[V].insert(H,DP[V+1][H])
                if len(DP[V])<3:
                    X=Worker("-", "FF", self.G)
                    DP[V].insert(1,X)
            if (DP[-1][0].Last or DP[-1][1].Last) and self.Checkvinc(DP):
                planning.append([[str(wk) for wk in wl] for wl in DP])
            DP=AL
        return planning
    
    #Check if the third counter is not empty
    def Checkvinc(self, DP):
        confirm=True
        CD=[L[-1] for L in DP]
        for wk in CD:
            confirm = False if wk.Name =="-" else confirm
        return confirm

    def DisplayPlanning(self, PL, S=False):
        PG=PL
        TL=self.G["Time_list"]
        DY=""
        for P in PG:
            DY+="#------------------Plan%s------------------#\n\t" %(PG.index(P)+1)
            DY+="\t".join(["00%s"% i for i in range(1,5) if i !=3])
            DY+="\n"
            for t in range(len(TL)):
                DY+="%s\t%s\t%s\t%s\n"%(TL[t], P[t][0], P[t][1], P[t][2])
            DY+="#-------------------End-------------------#\n"
            DY+="Rate: %s\n"%(self.Rate(P))
            DY+="$"
        DY=DY.split("$")
        #Sort by their marks
        DY.sort(key= lambda D:D[-3:])
        DY.reverse()
        DY="".join(DY)
        if S:
            print(DY)
        return DY
    
    def Rate(self, P):
        R=100
        L=[[]for i in range(3)]
        #Check if too many changes in the counter
        for H in range(3):
            L[H].append(P[0][H])
        for V in range(1,len(P)):
            for H in range(3):
                if P[V][H] not in ("-", L[H][-1]):
                    L[H].append(P[V][H])
        for K in L:
            R -= len(K)*5
        for W in self.Team:
            if W.Last is True:
                #Check if designated worker will take next counter in the middle of the day
                R=R+20 if W.Name in (P[len(P)//2 -1][2],) else R-5
        return R
                

    def DisplayTeam(self, T, P=False):
        TD="Name\t\tShift\t001\tBusy Hour\n%s\n" % ("".join("-" for i in range(0,50)))
        for wk in T:
            L=""
            B=",".join([str(W) for W in wk.BL])
            if wk.Last:
                L="YES"
            TD+="%s\t\t%s\t%s\t%s\n"%(wk.Name, wk.Shift, L, B)
        TD+="\nManpower today is %s\n"% ("/".join(self.MP))
        if P:
            print(TD)
        return(TD)            
                
class Worker:
    def __init__(self, Name,Shift, G, Last=False, Busy_list=None):
        if Busy_list==None:
            Busy_list=[]
        self.G=G
        self.TL=[i for i in self.G["Time_list"]]
        self.Name=str(Name)
        self.Shift=str(Shift)
        self.Last=bool(Last)
        self.BL=Busy_list
        self.WL=self.Revise(self.Shift, self.TL,self.BL)
        
    def Revise(self,shift, TL, BL):
        RL=self.G["Rest_list"][shift.strip()][0]
        RD=TL
        for time in RL:
            if time in TL:
                RD.remove(time)
        for time in BL:
            if time in TL:
                RD.remove(time)
        return RD

    def __repr__(self):
        return self.Name

